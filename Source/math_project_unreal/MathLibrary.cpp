// Fill out your copyright notice in the Description page of Project Settings.


#include "MathLibrary.h"

#include <cmath>
#include <numeric>


float UMathLibrary::RandomFloat(const float a, const float b) {
    return a + FMath::RandRange(0.0f, 1.0f) * (b - a);
}

int UMathLibrary::BernoulliDistribution(const float p) {
    return RandomFloat() < p ? 1 : 0;
}

int UMathLibrary::BinomialDistribution(const int n, const float p) {
    int result = 0;
    for (int i = 0; i < n; i++) {
        result += BernoulliDistribution(p);
    }
    return result;
}

float UMathLibrary::PoissonDistribution(const float most_likely_output) {
    float rand = RandomFloat();
    int n = 0;
    while (rand > exp(-most_likely_output)) {
            rand *= RandomFloat();
            n += 1;
        }
    return n;
}

float UMathLibrary::StandardNormalDistribution() {
    const float U1 = RandomFloat();
    const float U2 = RandomFloat();

    return sqrt(-2 * log(U1)) * cos(U2 * 2 * PI);
}

float UMathLibrary::NormalDistribution(const float mu, const float sigma) {
    return (mu + sigma * StandardNormalDistribution());
}

int UMathLibrary::GeometricDistribution(const float p) {
    int result = 0;
    int bernoulli = 0;
    while (!bernoulli) {
        bernoulli = BernoulliDistribution(p);
        result += 1;
    }
    return result;
}

FVector2D UMathLibrary::BoxMullerStandardNormalDistribution() {
    const float U1 = RandomFloat();
    const float U2 = RandomFloat();

    const float Z1 = sqrt(-2 * log(U1)) * cos(U2 * 2 * PI);
    const float Z2 = sqrt(-2 * log(U1)) * sin(U2 * 2 * PI);

    return {Z1, Z2};
}

FVector2D UMathLibrary::BoxMullerNormalDistribution(const FVector2D mu, const float sigma) {
    return mu + sigma * BoxMullerStandardNormalDistribution();
}

int UMathLibrary::MarkovBinomialDistribution(const TArray<float> p_list) {

    static BinomialMarkov binomiale = BinomialMarkov(p_list);
    return binomiale();
}