// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "Containers/Array.h"
#include "MathLibrary.generated.h"

/**
 *
 */
UCLASS()
class MATH_PROJECT_UNREAL_API UMathLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category = "MathLibrary")
		static float RandomFloat(const float a = 0.0f, const float b = 1.0f);

	UFUNCTION(BlueprintCallable, Category = "MathLibrary")
		static int BernoulliDistribution(const float p);

	UFUNCTION(BlueprintCallable, Category = "MathLibrary")
		static int BinomialDistribution(const int n, const float p);

	UFUNCTION(BlueprintCallable, Category = "MathLibrary")
		static float PoissonDistribution(const float most_likely_output);

	UFUNCTION(BlueprintCallable, Category = "MathLibrary")
		static float StandardNormalDistribution();

	 UFUNCTION(BlueprintCallable, Category = "MathLibrary")
	    static float NormalDistribution(const float mu, const float sigma);

	UFUNCTION(BlueprintCallable, Category = "MathLibrary")
		static int GeometricDistribution(const float p);

	UFUNCTION(BlueprintCallable, Category = "MathLibrary")
		static FVector2D BoxMullerStandardNormalDistribution();

	UFUNCTION(BlueprintCallable, Category = "MathLibrary")
		static FVector2D BoxMullerNormalDistribution(const FVector2D mu, const float sigma);

	UFUNCTION(BlueprintCallable, Category = "MathLibrary")
		static int MarkovBinomialDistribution(const TArray<float> p_list);

};



class BinomialMarkov {
public:
	BinomialMarkov(const TArray<float> list) : p_list(list) {};
	~BinomialMarkov() = default;
	int operator()() {
		const auto ret = UMathLibrary::BinomialDistribution(p_list.Num() - 1, p_list[previous]);
		previous = ret;
		return ret;
	}
private:
	int previous{0};
	const TArray<float> p_list;
};